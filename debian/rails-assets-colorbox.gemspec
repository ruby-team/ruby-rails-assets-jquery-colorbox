# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails-assets-jquery-colorbox/version'

Gem::Specification.new do |spec|
  spec.name          = "rails-assets-colorbox"
  spec.version       = RailsAssetsJqueryColorbox::VERSION
  spec.authors       = ["rails-assets.org"]
  spec.description   = "jQuery lightbox and modal window plugin"
  spec.summary       = "jQuery lightbox and modal window plugin"
  spec.homepage      = "http://www.jacklmoore.com/colorbox"

  spec.files         = ''
  spec.require_paths = ["lib"]

  spec.add_dependency "rails-assets-jquery", ">= 1.3.2"
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
